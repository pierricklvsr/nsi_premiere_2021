from IPython.display import clear_output
from ipywidgets import *
import random as rd


TO = "joliot10.tison.ludovic@gmail.com"

name = ''
seed = 0
valide = False

go = False



nbr_exos = 11
list_score = [0 for _ in range(nbr_exos)]
exos_faits = [0 for _ in range(nbr_exos)]
score_max = [1,1,1,1,1,2,2,2,3,3,3]

def identification():
    global name
    global seed
    print("écrivez vos nom et prenom")
    
    aaa=widgets.Text(
    value='',
    placeholder='',
    description='',
    disabled=False
    )
    butt = widgets.Button(description='valider_nom')
    
    def on_clicked(b):

        clear_output()
        global name
        global seed
        seed = 0
        name = aaa.value
        for l in name:
            seed += ord(l)
        print(f'Bienvenue {name}')
        
    aaa.on_submit(on_clicked)    
    butt.on_click(on_clicked)
    return widgets.VBox([aaa,butt])
        
        


def exo1():
    global exos_faits
    global list_score
    
    global go
    try:        
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed)

        a = rd.randint(-10,10)
        b = rd.randint(-10,10)
        c = a + b
        
        print('                  ')
        print(f'a <-- {a}')
        print(f'b <-- {b}')
        print(f'c <-- a + b')
        print('                  ')
        print(f'que vaut c ?')   


        aaa=widgets.Text(
        value='',
        placeholder='',
        description='Réponse:',
        disabled=False
        )

        butt = widgets.Button(description='Valider')
        outt = widgets.Output()

        def on_clicked(b):
            global exos_faits
            global go
            go = False
            with outt:
                clear_output()
                try:
                    int(aaa.value)
                    if int(aaa.value) == c:
                        list_score[0] = 1
                    print('reponse enregistrée')
                    exos_faits[0] = 1

                except:
                    print('il faut saisir un nombre entier')
        aaa.on_submit(on_clicked)    
        butt.on_click(on_clicked)
        return widgets.VBox([aaa,butt,outt])
    except:
        print("veuillez vous identifier")

def exo2():
    global exos_faits
    
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+1)     
        a = rd.randint(-10,10)
        b = rd.randint(-10,10)
        c = (a + b) * (a - b)

        print('                  ')
        print(f'a <-- {a}')
        print(f'b <-- {b}')
        print(f'c <-- (a + b)(a - b)')
        print('                  ')
        print(f'que vaut c ?')   


        aaa=widgets.Text(
        value='',
        placeholder='',
        description='Réponse:',
        disabled=False
        )

        butt = widgets.Button(description='Valider')
        outt = widgets.Output()

        def on_clicked(b):
            global exos_faits
            global go
            go = False
            with outt:
                clear_output()
                try:
                    int(aaa.value)
                    if int(aaa.value) == c:
                        list_score[1] = 1
                    print('reponse enregistrée')
                    exos_faits[1] = 1
                    
                except:
                    print('il faut saisir un nombre entier')
        aaa.on_submit(on_clicked)    
        butt.on_click(on_clicked)
        return widgets.VBox([aaa,butt,outt])
    except:
        print("veuillez vous identifier")



def exo3():
    global exos_faits
    
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+2)     
        a = rd.randint(-10,10)
        b = rd.randint(-10,10)

        print('                  ')
        print(f'fonction(a,b)')
        print(f'    c <-- a + b')
        print(f'    c <-- c * 5')
        print('     afficher c                  ')
        print(f'que va afficher fonction({a},{b}) ?')  

        c = (a + b) * 5

        aaa=widgets.Text(
        value='',
        placeholder='',
        description='Réponse:',
        disabled=False
        )

        butt = widgets.Button(description='Valider')
        outt = widgets.Output()

        def on_clicked(b):
            global exos_faits
            global go
            go = False
            with outt:
                clear_output()
                try:
                    int(aaa.value)
                    if int(aaa.value) == c:
                        list_score[2] = 1
                    print('reponse enregistrée')
                    exos_faits[2] = 1
                    
                except:
                    print('il faut saisir un nombre entier')
        aaa.on_submit(on_clicked)    
        butt.on_click(on_clicked)
        return widgets.VBox([aaa,butt,outt])
    except:
        print("veuillez vous identifier")
        
def exo4():
    global exos_faits
    
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+3)     
        a = rd.randint(10,30)
        b = rd.randint(0,10)

        print('                  ')
        print(f'pour i allant de {b} à {a}')
        print(f'    salut')
        print('                  ')
        print('Combien de fois "salut" va-t-l s\'afficher?') 
        print('                  ')
        

        c = a - b

        aaa=widgets.Text(
        value='',
        placeholder='',
        description='Réponse:',
        disabled=False
        )

        butt = widgets.Button(description='Valider')
        outt = widgets.Output()

        def on_clicked(b):
            global exos_faits
            global go
            go = False
            with outt:
                clear_output()
                try:
                    int(aaa.value)
                    if int(aaa.value) == c:
                        list_score[3] = 1
                    print('reponse enregistrée')
                    exos_faits[3] = 1
                    
                except:
                    print('il faut saisir un nombre entier')
        aaa.on_submit(on_clicked)    
        butt.on_click(on_clicked)
        return widgets.VBox([aaa,butt,outt])
    except:
        print("veuillez vous identifier")
        
def exo5():
    global exos_faits
    
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+4)     
        a = rd.randint(-20,100)
        b = rd.randint(-10,20)
        
        print('On considère le code Python suivant:')

        print('                  ')
        print(f'a = {a}')
        print(f'b = {b}')
   
        print('if ( a > 2 * b) :')
        print('    c = a + 2 * b')
        print('    c = c - 3')
        print('else :')
        print('    c = a - b')
        print('                  ')
        print('Que vaut c ?') 
        print('                  ')
        

        if a > 2 * b:
            c = a + 2 * b
            c = c - 3
        else:
            c = a - b

        aaa=widgets.Text(
        value='',
        placeholder='',
        description='Réponse:',
        disabled=False
        )

        butt = widgets.Button(description='Valider')
        outt = widgets.Output()

        def on_clicked(b):
            global exos_faits
            global go
            go = False
            with outt:
                clear_output()
                try:
                    int(aaa.value)
                    if int(aaa.value) == c:
                        list_score[4] = 1
                    print('reponse enregistrée')
                    exos_faits[4] = 1
                    
                except:
                    print('il faut saisir un nombre entier')
        aaa.on_submit(on_clicked)    
        butt.on_click(on_clicked)
        return widgets.VBox([aaa,butt,outt])
    except:
        print("veuillez vous identifier")
        
def exo6(fonc):
    num_exo = 6
    global exos_faits
    
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+5)     
        test = [ rd.randint(-100,100) for i in range(10) ]
        res = [i+5 for i in test ]
        try:
            for i in range(len(test)):
                assert res[i] == fonc(test[i])
            list_score[num_exo-1] = 2
        except:
            pass
        print('réponse enregistrée')
        exos_faits[num_exo-1] = 1
        
    except:
        print("veuillez vous identifier")
        
def exo7(fonc):
    num_exo = 7
    global exos_faits
    
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+6)     
        test = ["où", "où se trouve", "dueutu", "ordinateür", "ralalala", "ubù", "uùü","uuuuuuuuuuuuu"]
        res = [1,2,3,1,0,2,3,13]
        try:
            for i in range(len(test)):
                assert res[i] == fonc(test[i])
            list_score[num_exo-1] = 2
        except:
            pass
        print('réponse enregistrée')
        exos_faits[num_exo-1] = 1
        
    except:
        print("veuillez vous identifier")
        
def exo8(fonc):
    num_exo = 8
    global exos_faits
    
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+7)     
        test = [rd.randint(0,100) for i in range(100)]
        res = [i*(i+1)/2 for i in test]
        try:
            for i in range(len(test)):
                assert res[i] == fonc(test[i])
            list_score[num_exo-1] = 2
        except:
            pass
        print('réponse enregistrée')
        exos_faits[num_exo-1] = 1
        
    except:
        print("veuillez vous identifier")
        
def exo9(fonc):
    num_exo = 9
    global exos_faits
    global seed
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+8)     
        test = [2000,2004,2001,3000,2400,2200,2002,2003]
        res = [366,366,365,365,366,365,365,365]
        try:
            for i in range(len(test)):
                assert res[i] == fonc(test[i])
            list_score[num_exo-1] = 3
        except:
            pass
        print('réponse enregistrée')
        exos_faits[num_exo-1] = 1
        
    except:
        print("veuillez vous identifier")
        
def exo11(fonc):
    
    num_exo = 11
    global exos_faits
    
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+10)     
        test = [9,2020,1983,9999,18949816]
        res = [1,0,3,1,4]
        try:
            for i in range(len(test)):
                assert res[i] == fonc(test[i])
            list_score[num_exo-1] = 3
        except:
            pass
        print('réponse enregistrée')
        exos_faits[num_exo-1] = 1
        
    except:
        print("veuillez vous identifier")
        
def exo10(fonc):
    
    num_exo = 10
    global exos_faits
    global seed
    global go
    try:
        assert seed != 0, "veuillez vous identifier"
        rd.seed(seed+9)     
        test = [1,2,4,8,1024,96,320,111]
        res = [0,1,2,3,10,5,6,0]
        try:
            for i in range(len(test)):
                assert res[i] == fonc(test[i])
            list_score[num_exo-1] = 3
        except:
            pass
        print('réponse enregistrée')
        exos_faits[num_exo-1] = 1
        
    except:
        print("veuillez vous identifier")
        
    



        



    
def valid2():
    global FROM,TO,PASSWORD
    global list_score
    global name
    global valide
    global seed
    global exos_faits
    global go
    #print(' ENREGISTREZ AVANT DE VALIDER !!!!')
    try:
        assert seed != 0
        try:
            assert valide == False, "vous avez déjà validé"
            score = sum(list_score)
            butt = widgets.Button(description='valider score')
            
            def on_clicked(b):
                
                global exos_faits
                global nbr_exos
                global valide
                global go
                if nbr_exos == sum(exos_faits) or go:
                    clear_output()
                    try:
                        assert valide == False, "vous avez déjà validé"
                        valide = True
                        try:
                            for i in range(nbr_exos):
                                print(f'exo{i+1}: {list_score[i]}/{score_max[i]}')
                            print(f'Pensez à re-sauvegarder et envoyer ce notebook par mail à {TO}')
                        except Exception as e:
                            print("problème dans l'envoie du mail", e)
                    except:
                        print('vous avez déjà validé')
                else:
                    print("Vous n'avez pas répondu à toutes les questions, valider quand même?")
                    go = True

            butt.on_click(on_clicked)
            return widgets.VBox([butt])
        except:
            print("vous avez déjà validé")
    except:
        print("veuillez vous identifier")
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    