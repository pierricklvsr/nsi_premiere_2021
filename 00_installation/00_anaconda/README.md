## Installation de Anaconda

### Présentation:

Anaconda est un ensemble de logiciel servant à la programmation en python.

Dans anaconda nous utiliserons Jupyter Notebook, qui sert à la fois d'éditeur de texte et de console entre autres fonctionnalités.

### Téléchargement:

 Télécharger Anaconda en vous rendant sur https://www.anaconda.com/products/individual

 Chercher la section __Anaconda Installers__ en descendant sur la page ou bien en ouvrant une fenêtre de recherche "Ctrl + F" (peut-être différent sur portable) et en tapant "installers".

 Choisir "Windows" puis "64-bit Graphical Installer" .

 ### Installation:

Exécuter le fichier téléchargé.

CLiquer sur "Next" (ou suivant) jusqu'à obtenir le choix du répertoire d'installation.

Cliquer sur "Browse..." (ou parcourir)

Choisir "Ce PC" puis "Disque C:"

Cliquer sur "créer un nouveau dossier" que vous appelerez "anaconda"

Le selectionner puis cliquer sur "OK"

Assurez-vous d'avoir bien "C:/anaconda" de sélectionné, sinon il faudra sûrement recommencer l'installation.

Cliquer sur "Next" une seule fois!!!

Cochez la case "ADD TO PATH", sinon il faudra sûrement recommencer l'installation. Cela rendra accessible anaconda partout sur votre machine.

Laissez l'autre case cochée afin d'installer le noyau python 3.8 sur votre machine.

Terminer l'installation en cliquant sur "Next".

### Test

Une fois l'installation terminée, redémarrer l'ordinateur

Une fois redémarré, appuyer sur la touche windows de votre clavier (en bas à gauche la touche avec 4 carrés)

Taper "power" pour lancer le powershell de windows.

Le powershell de windows permet d'entrer des lignes de commandes sous windows.

Testez votre installation en tapant:

```
jupyter notebook
```

Si on vous indique que la commande n'est pas connue, vous n'avez pas dû cocher la case "ADD TO PATH" et il faut désinstaller puis réinstaller anaconda, sinon passer à la suite.




 
