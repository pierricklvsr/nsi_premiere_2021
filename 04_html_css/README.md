### Liens vers les sites élèves:

https://gruu.gitlab.io/dessin_alis/site_internet.html  

https://steven_20xx.gitlab.io/sardoche/page1.html  

https://maximemoi.gitlab.io/webpage_project_1/home.html  

https://le_pinguin_.gitlab.io/page-harry-potter/page_1_bio.html  

https://emmabaumann.gitlab.io/premiere_page/premiere_page.html  

https://gabrielh17.gitlab.io/premier-projet/home.html  

https://gabrielle_jln.gitlab.io/les-dieux-egyptiens/les_dieux_egyptiens.html

### Projet

Il s'agit de construire un site web à 1 ou 2 maximum sur un thème libre.
 * utilisation de gitlab
 * Au moins deux pages reliées par des liens
 * insertion d'une ou plusieurs images
 * au moins deux niveaux de titre
 * des couleurs (bien choisies)
 * une liste
 * un lien vers une référence extérieure
 * une personnalisation du CSS (police, couleur, mise en page, ...)
 * HTML avec entête propre


