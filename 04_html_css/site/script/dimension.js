function dimension()
    {
        /* Cette fonction récupère les hauteurs du bandeau fixe,
        c'est à dire des trois premiers div de la page
        et la transmet aux propriétés CSS pour que l'affichage 
        se fasse correctement avec le bandeau fixe.
        */
        var hd =  document.getElementsByTagName('div');
        var hauteur1 = hd[0].offsetHeight ;
        var hauteur2 = hd[1].offsetHeight ;
        var hauteur3 = hd[2].offsetHeight ;
        var decalage = Number(hauteur1)+Number(hauteur2)+Number(hauteur3);
        var principal = document.getElementById('principal');
        principal.style.marginTop = String(decalage) +'px' ;
        var cibles = document.getElementsByClassName("cible");
        for(var i=0 ; i<cibles.length ; i++ )
            {
            cibles[i].style.top = '-'+String(decalage)+'px';
            }
    }

// La fonction dimension est appelée au chargement de la parge et chaque faois que la fenêtre est redimentionnée
window.onload = dimension ;
window.onresize = dimension ;    

function verifier1() {
    /* Cette fonction permet la validation du formulaire de l'exercice 1.
    */
    var formulaire = document.getElementById("question1");
    var rep = formulaire.getElementsByTagName("select");
    var txtReponse = formulaire.getElementsByTagName("input");
    var bonnesReponses = 0 ;
    if (rep[0].value === "Titre de niveau 1 <h1>") {
        bonnesReponses++;
    }
    if (rep[1].value === "Titre de niveau 2 <h2>") {
        bonnesReponses++;
    }
    if (rep[2].value === "Paragraphe <p>") {
        bonnesReponses++;
    }
    if (rep[3].value === "Paragraphe <p>") {
        bonnesReponses++;
    }
    if (rep[4].value === "Titre de niveau 2 <h2>") {
        bonnesReponses++;
    }
    if (rep[5].value === "Titre de niveau 3 <h3>") {
        bonnesReponses++;
    }
    if (rep[6].value === "Paragraphe <p>") {
        bonnesReponses++;
    }
    if (rep[7].value === "Paragraphe <p>") {
        bonnesReponses++;
    }
    if (rep[8].value === "Titre de niveau 3 <h3>") {
        bonnesReponses++;
    }
    if (rep[9].value === "Paragraphe <p>") {
        bonnesReponses++;
    }
    if (bonnesReponses === 10) {
        txtReponse[0].value = "Bravo, vous avez toutes les bonnes réponses !";    
    } else {
        if (bonnesReponses === 1) {
            txtReponse[0].value = "Vous n'avez qu'une bonne réponse sur 10. Vérifiez..."
        } else {
            if (bonnesReponses === 0) {
                txtReponse[0].value = "Vous n'avez aucune bonne réponse sur 10. Vérifiez..."
            } else {
                txtReponse[0].value = "Vous n'avez que " + String(bonnesReponses) + " bonnes réponses sur 10. Vérifiez..."
            }
        }
    }
}    



